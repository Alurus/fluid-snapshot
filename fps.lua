-- build 10
--
-- fps.lua
--
-- Author: Tom Newman
--
-- Used to display the FPS, Texture, and Lua Memory used on Display or Console
--
-- Calling showFps() returns a group Display Object and starts the display
-- Skips the first 5 readings to avoid messing up the min/max values
-- Computes min/max value of the last 100 readings
-- Automatically adjusts for current FPS setting (display.fps)
--
-- Screen/console refresh count defaults to every 10 enterFrames (refreshCount)
-- Defaults to showing FPS on screen unless parameter "console = false"
-- Displays elasped time app has been running (m.s)  using "timeShow" option
--
----------------------------------------------------------------------------------
--
-- Options:
--		gcShow = true		- Shows Lua memory usage (default is false)
--      refreshCount = n    - Number of frames before the display is refreshed (dafault is 10)
--      console = true      - Show values in the console (print statements) only (default is false)
--      timeShow = true     - Show time (m.s) value at end of display line (default = false)
------------------------------------------------------------
-- Usage:
--	  local fps = require( "myLibrary.fps" )
--	  fps:showFps( { gcShow = true, console = true, refreshCount = 30, timeShow = true } )
-- -or-
--	  local fps = require( "fps" )
--	  fps:showFps()						-- Start FPS display without GC memory
-- -or-
--    local fps = require( "fps" )
--    fps:showFps( { gcShow = true } )	-- Start FPS display with GC display
--    fps:showFps( { refreshCount = 20, gcShow = true } )	-- Start FPS display with GC display, refresh count
--    fps:showFps( { gcShow = true, timeShow = true } )	-- Start FPS display with GC display and elsasped time
--	----------------------------------------------------------
-- -move and scale display-
--    fps.x = fps.x + 10				-- move the display
--    fps.y = fps.y + 20
--    fps:scale(0.75, 0.75 )			-- scale it smaller
--
-- -Console only mode (for screen display)
--    local fps = require( "fps" )
--	  fps:showFps( { console = true } )
--	  fps:showFps( { console = true, refreshCount = 20 } )
--	  fps:showFps( { console = true, refreshCount = 20, timeShow = true } )
-------------------------------------------------------------------------
-- History
--	8/16/2012		Converted from fps.lua and added misc. enhancements
--	10/21/2012		Added "console" mode for console output
--					Added "refreshCount" to determine how open to update
--  2/18/2014		Updated for Graphics 2.0
--  3/31/2014		Added time display (minutes.seconds) at end of display
--  4/16/2014		Fixed up "console" mode (displays memory usage)
--  5/28/2014		Fixed runtime error when "parms" is missing
--	6/10/2014		Fixed text underlay to match text position
--					Increased underlay size
------------------------------------------------------------------------

local consoleFlag = false		-- set True for console-only output
local refreshCount = 10			-- update rate (number of enterFrames times)
local timerCountdown			-- temporary countdown for enterframe
local timeShow = false 			-- show Time (m.s) at end of display
local SKIP_COUNT = 5			-- number of cycles to wait before storing results
local waitCount = 0

local gcShow = false			-- show Garbage Collected (Lua) memory
local textSize = 14

local prevFps = 0
local MAX_HISTORY = 100			-- total FPS values to store (used for high/low)
local history = {}				-- stores our past FPS values

local historyIndex = 1

local displayGroup = display.newGroup()
-- print( displayGroup )

function displayGroup:showFps( parms )

	------------------------------------------------------------
	-- Look at any parameters passed

	-- Enable GC Memory display if parameter is "true"
	if parms and parms.gcShow ~= nil then
		-- print( "gcShow: ", parms.gcShow )
		gcShow = parms.gcShow
	end
	
	consoleFlag = (parms and parms.console) or consoleFlag
	refreshCount = (parms and parms.refreshCount) or refreshCount
	timeShow = (parms and parms.timeShow) or timeShow
	
	local parms = parms or {}		-- create tabel if it doesn't exist
	-- print( "Params: ", 	parms.gcShow, parms.console, parms.refreshCount, gcShow )
	------------------------------------------------------------
	
	timerCountdown = refreshCount	-- Reset Timer countdown
	
	local prevTime = 0
	local prevFps = 0
	local displayInfo
	
	if not consoleFlag then
		local underlay = display.newRect(0, 0, 300, 20)
		underlay.x = 5
		underlay.y = 30
		
		-- Code to get the current Graphics version
		-- pcall is used because display.getDefault doesn't exist on Graphics 1 simulator
		--
		local flag, gVer = pcall( display.getDefault, "graphicsCompatibility" )
--		print( "Graphics: ", flag, gVer )
		
		displayGroup:insert( underlay )

		-- Create a placeholder for our displayed text
		displayInfo = display.newText("FPS:  0,  0,  0 - 0.00/0.00 mb", 60, 5,
			native.systemFontBold, textSize)
		
		displayInfo.y = underlay.y + 2
		displayInfo.x = underlay.x + 5
		displayGroup:insert( displayInfo )

		if flag and gVer == 2 then
			displayInfo.anchorX = 0.0
			displayInfo.anchorY = 0.0
			underlay.anchorX = 0.0
			underlay.anchorY = 0.0				
			underlay:setFillColor(0, 0, 0, 128/255)    	-- G2
		else
			displayInfo:setReferencePoint(TopLeftReferencePoint)
			underlay:setReferencePoint(TopLeftReferencePoint)
			underlay:setFillColor(0, 0, 0, 128)   		-- G1
		end
	end	

	-- Loop: Update FPS and Texture/Lua Memory displays
	--
	-- Uses timerCountdown to limit the display update
	--
	local function updateText()
		
		local gcMem, newFps
		local curTime = system.getTimer()
		local dt = curTime - prevTime
		prevTime = curTime
		local fps = math.floor(1000 / dt)
		local mem = system.getInfo("textureMemoryUsed") / 1000000
		
		if gcShow then
			gcMem = string.format( "/%3.2f", collectgarbage( "count" )/1000 )
		else
			gcMem = ""
		end
		
		--Limit fps range to avoid the "fake" reports
		if fps > display.fps then
			fps = display.fps
		end
		
		-- Weight the FPS reading with the previous reading
		if waitCount < SKIP_COUNT then
			newFps = fps 	-- **temp
		else
			newFps = 0.05*fps + 0.95*prevFps
		end
		 
		prevFps = newFps
		
		history[historyIndex] = newFps
		local minFps = history[1]
		local maxFps = history[1]
		
		-- Check to see if we need to start storing values
		-- Else delay until we reach the wait count
		--
		if waitCount < SKIP_COUNT then
			waitCount = waitCount + 1		-- advance our wait count
		else

			historyIndex = historyIndex + 1
			
			if ( historyIndex > MAX_HISTORY ) then
--				print( "history = 1" )
			  historyIndex = 1
			end
			
			if #history >= 3 then
			
				for i = 1, #history do
					local v = history[i]
--					print( "value = " .. v, i )
					if ( v < minFps ) then minFps = v; end
					if ( v > maxFps ) then maxFps = v; end
				end
			  
			end
			
		end -- waitCount
	
		-- Check if we need to update the console/display
		timerCountdown = timerCountdown - 1
		
		if timerCountdown > 0 then
			return
		end
		
		timerCountdown = refreshCount	-- Reset Timer countdown
		
		-- Get the elasped time app has been running
		local timeRunning = ""

		if timeShow then
			timeRunning = string.format( " (%2.2f t)", system.getTimer()/1000/60 )
		end

		local str = string.format( "%2.0f, %2.0f, %2.0f", newFps, minFps, maxFps )


		if consoleFlag then
			print( "FPS, min, max: " .. str .. string.format( " - %4.2f", mem ) ..
					gcMem .. " mb" .. timeRunning )
		else
			displayInfo.text = "FPS: " .. str .. string.format( " - %4.2f", mem ) ..
					gcMem .. " mb" .. timeRunning
			displayGroup:toFront()
		end
		
	end
		
	Runtime:addEventListener("enterFrame", updateText)
	
--	return displayGroup
end
	
	return displayGroup