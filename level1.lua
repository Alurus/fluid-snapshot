---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )



local camera, bikeWheel, menuBtn
---------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    -- print("Level 1 created")
    -- Called when the scene's view does not exist
    -- 
    -- INSERT code here to initialize the scene
    -- e.g. add display objects to 'sceneGroup', add touch listeners, etc
        bikeWheel = self:getObjectByName("bikeWheel")
        local fWheel = self:getObjectByName("fWheel")
        local bFrame = self:getObjectByName("bikeFrame")

        local pivotJoint = physics.newJoint( "pivot", bFrame, fWheel, fWheel.x, fWheel.y )
        local pivotJoint2 = physics.newJoint( "pivot", bFrame, bikeWheel, bikeWheel.x, bikeWheel.y )

        --------------------------------------------------------------------------------
         -- Build Camera
        --------------------------------------------------------------------------------
        local perspective = require("perspective")  -- Code Exchange library to manage camera view
        camera = perspective.createView()
        sceneGroup:insert(camera)

        local goBtn = self:getObjectByName("goBtn")
        local revBtn = self:getObjectByName("revBtn")
        local menuBtn = self:getObjectByName("menuBtn")

        function pressFwd(bikeWheel,event)
            bikeWheel:applyTorque( 3500 )
            -- print("run")
        end

        function pressRev(bikeWheel,event)
            bikeWheel:applyTorque( -3500 )
            -- print("run")
        end

        function goBtn:touch( event )
            if event.phase == "began" then
                Runtime:addEventListener("enterFrame", bikeWheel)
                bikeWheel.enterFrame = pressFwd
                display.getCurrentStage():setFocus( self )
                self.isFocus = true
                -- print( "You touched the fwd!" )
            elseif event.phase == "ended" then
                Runtime:removeEventListener("enterFrame", bikeWheel)
                -- print( "You ended the touch to fwd!" )
                display.getCurrentStage():setFocus( nil )
                self.isFocus = nil
            end
            return true
        end
        
        function revBtn:touch( event )
            if event.phase == "began" then
                bikeWheel.enterFrame = pressRev
                -- print( "You touched the rev!" )
                Runtime:addEventListener("enterFrame", bikeWheel)
                display.getCurrentStage():setFocus( self )
                self.isFocus = true
            elseif event.phase == "ended" or event.phase == "cancelled" then
                -- print( "You ended the touch to rev!" )
                Runtime:removeEventListener("enterFrame", bikeWheel)
                display.getCurrentStage():setFocus( nil )
                self.isFocus = nil
            end   
            return true
        end
        goBtn:addEventListener( "touch", goBtn )
        revBtn:addEventListener( "touch", revBtn )


        
        -- Only start the camera tracking when the object has moved pasted the center of the view port
        function trackBike( event )
            if bFrame then
                camera:setFocus(bFrame) -- Set the focus to the object so it tracks it
                Runtime:removeEventListener( 'enterFrame', trackBike )
                camera:track()
            end
        end



















        local circle = {}
        local background
        local floor
        local leftWall
        local rightWall
        local platform1
        local platform2
        local tmr
        local rand
        local snapshot
        local snapshotGroup


        snapshot = display.newSnapshot(2500, 4000)
        snapshot.anchorX = 0.5
        snapshot.anchorY = 0.5
        snapshot.x, snapshot.y = 0, 0

        snapshotGroup = snapshot.group

        snapshot.fill.effect = "filter.levels"
        snapshot.fill.effect.white = 0.7
        --snapshot.fill.effect.black = 0.1
        --snapshot.fill.effect.gamma = 0



        -- physics.start() 
        -- physics.setDrawMode("hybrid")



        function spawnFn()

          for i= 1, 6 do
          
        rand = math.random(12,32)

        circle[#circle+1] = display.newImageRect("drop.png", rand, rand )
        circle[#circle].anchorX = 0.5
        circle[#circle].anchorY = 0.5
        circle[#circle].x = 300
        circle[#circle].y = 100
        -- cirlce[#circle].fill( 255)
         -- circle[#circle]:scale(.5,.5)

        snapshotGroup:insert( circle[#circle])


        physics.addBody(circle[#circle], {density=7, friction = 0, bounce = 0.2, radius = rand/math.random(6,8)})

          end
          
        end









    tmr = timer.performWithDelay(100,spawnFn,30) 

    Runtime:addEventListener("enterFrame", function() snapshot:invalidate() end)














































        Runtime:addEventListener( 'enterFrame', trackBike )
       
        local gameGroup = self:getObjectByName( "gameGroup" )
        local bikeGroup = self:getObjectByName( "bikeGroup" )
        local A = self:getObjectByName("A")
        local B = self:getObjectByName("B")
        local C = self:getObjectByName("C")

        -- gameGroup:insert(A)
        -- gameGroup:insert(B)
        -- gameGroup:insert(C)
        gameGroup:insert(snapshotGroup)
        ------------------------------------------------------------
        -- Camera code
        -- gameGroup:insert(snapshotGroup)
        -- -- gameGroup:insert(circle)
        -- --camera:setParallax(1,.6)
        -- -- camera:add(snapshotGroup)
        -- camera:add( gameGroup, 4)   
        -- camera:add( bikeGroup, 1)        
        
        -- camera:setBounds( 0, 5635, 320, 320 ) -- used to keep our view from going outside the background
        -- print (camera:layerCount())
        ------------------------------------------------------------
        menuBtn = self:getObjectByName( "menuBtn" )
        if menuBtn then
            -- touch listener for the button
            function menuBtn:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                    goBtn:removeEventListener( "touch", goBtn )
                    revBtn:removeEventListener( "touch", revBtn )
                    Runtime:removeEventListener("enterFrame", bikeWheel)
                    Runtime:removeEventListener( 'enterFrame', trackBike )
                    camera:destroy()
                    composer.gotoScene( "scene1", { effect = "fade", time = 100 } )
                    menuBtn:removeEventListener( "touch", menuBtn )
                end
            end
            -- add the touch event listener to the button
            menuBtn:addEventListener( "touch", menuBtn )
        end
































end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen

    elseif phase == "did" then
        -- print("Level 1 did show")
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
        
        -- we obtain the object by id from the scene's object hierarchy
        
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)

    elseif phase == "did" then
        -- print("level 1 did hide")
        -- Called when the scene is now off screen

    end 
end


function scene:destroy( event )
    local sceneGroup = self.view
        display.remove ( camera )
        camera = nil  --set reference to nil!
        display.remove ( menuBtn )
        menuBtn = nil  --set reference to nil!
        display.remove ( bikeWheel )
        bikeWheel = nil  --set reference to nil!
        Runtime:removeEventListener("enterFrame", bikeWheel)
        Runtime:removeEventListener( 'enterFrame', trackBike )
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc

    -- print("level 1 destroyed")
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
