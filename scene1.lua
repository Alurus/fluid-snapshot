---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

-- Enable auto-recycle on scene change
composer.recycleOnSceneChange = true
-- composer.removeScene( "level1", true )
-- composer.removeScene( "level2", true )
-- composer.removeScene( "level3", true )
---------------------------------------------------------------------------------



function scene:create( event )
    local sceneGroup = self.view

    -- Called when the scene's view does not exist
    -- 
    -- INSERT code here to initialize the scene
    -- e.g. add display objects to 'sceneGroup', add touch listeners, etc
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen

    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
        
        -- we obtain the object by id from the scene's object hierarchy


        local level1Btn = self:getObjectByTag( "level1Btn" )
        if level1Btn then
            -- touch listener for the button
            function level1Btn:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                    composer.gotoScene( "level1" )
                    level1Btn:removeEventListener( "touch", level1Btn )
                end
            end
            -- add the touch event listener to the button
            level1Btn:addEventListener( "touch", level1Btn )
        end


        local level2Btn = self:getObjectByTag( "level2Btn" )
        if level2Btn then
            -- touch listener for the button
            function level2Btn:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                    composer.gotoScene( "level2", { effect = "fade", time = 60 } )
                    level2Btn:removeEventListener( "touch", level2Btn )
                end
            end
            -- add the touch event listener to the button
            level2Btn:addEventListener( "touch", level2Btn )
        end


        local level3Btn = self:getObjectByTag( "level3Btn" )
        if level2Btn then
            -- touch listener for the button
            function level3Btn:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                    composer.gotoScene( "level3", { effect = "fade", time = 60 } )
                    level3Btn:removeEventListener( "touch", level3Btn )
                end
            end
            -- add the touch event listener to the button
            level3Btn:addEventListener( "touch", level3Btn )
        end

        -- timer.performWithDelay( 1, function() 
        --     composer.removeHidden( true )
        -- end)
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
