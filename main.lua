---------------------------------------------------------------------------------
--
-- main.lua
--
---------------------------------------------------------------------------------
local fps = require( "fps" )
--    fps:showFps( { gcShow = true } )	-- Start FPS display with GC display
--    fps:showFps( { refreshCount = 20, gcShow = true } )	-- Start FPS display with GC display, refresh count
  fps:showFps( { gcShow = true, timeShow = true } )	-- Start FPS display with GC display and elsasped time
  fps.x = fps.x + 300				-- move the display
  fps.y = fps.y + 20
  fps:scale(2, 2 )			-- scale it smaller
-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )
local physics = require("physics")
physics.start()
-- physics.setDrawMode( "hybrid" )
-- physics.setContinuous( false )
-- require the composer library
local composer = require "composer"
  composer.recycleOnSceneChange = true
-- load scene1
composer.gotoScene( "level1" )
-- composer.isDebug = true
-- Add any objects that should appear on all scenes below (e.g. tab bar, hud, etc)

 